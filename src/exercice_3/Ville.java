package exercice_3;
import exercice_1_2.Liste;

public class Ville {
	private String nom;
	private Liste villesJoignables;
	
	/**
	 * Constructeur prenant le nom en param�tre
	 * 
	 * @author Boffy William
	 */
	Ville(String n){
		this.nom = n;
		this.villesJoignables = new Liste(this);
	}
	
	/**
	 * Accesseur sur l'attribut nom
	 * 
	 * @author Boffy William
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Permet de savoir si une ville pass�e en param�tre est accessible par l'objet courant
	 * 
	 * @author Boffy William
	 * @param Ville other, une ville dont l'accessibilit� est � tester
	 * @return bool�en - si accessible true, sinon false
	 */
	public boolean estJoignable(Ville other) {
		return this.villesJoignables.contient(other);
	}
	
	/**
	 * Ajout d'une ville � la liste des villes accessibles par l'objet courant
	 * 
	 * @author Boffy William
	 * @param Ville v, une ville � ajouter aux villes accessibles
	 */
	public void update(Ville v) {
		//on souhaite simplement ajouter la ville si elle est d�j� accessible. Sinon on ajoute l'ensemble du nouveau r�seau de ville associ�.
		if(!this.estJoignable(v)) {
			//On stocke l'ensemble des nouvelles villes accessibles par le nouveau r�seau de l'objet courant
			Liste tmp = v.villesJoignables.concat(this.villesJoignables);
			//L'objet courant conna�t d�j� l'ensemble du r�seau complet mais pas encore les villes associ�es � la ville ajout�e
			Liste current = v.villesJoignables.copie();
			while(current!=null) {
				//on parcourt du r�seau � mettre � jour les villes associ�es � la ville ajout�e au r�seau courant
				Ville pt = (Ville) current.tete();
				pt.villesJoignables = tmp;
				current = current.precedent();
			}
		}
	}
	
	/**
	 * Retourne la liste des villes accessibles
	 * 
	 * @author Boffy William
	 * @return villesJoignables, les villes accessibles
	 */
	public Liste getVillesJoignables() {
		return this.villesJoignables;
	}
	
	@Override
	public String toString() {
		return this.getNom();
	}
}
