package exercice_5;
import java.util.ListIterator;
import java.util.Vector;

public class Album {
	private Vector<MyImage> images;
	private ListIterator<MyImage> itr;
	final static String FOLDER = "C:\\Users\\willi\\Pictures\\Camera Roll\\Voyages\\R�union\\JPG";
	
	/**
	    Constructeur d'une instance d'Album.
	*/
	public Album() {
		this.images = new Vector<MyImage>();
		this.itr = this.images.listIterator();
	}
	
	public Vector<MyImage> getImages() {
		return images;
	}
	public void setImages(Vector<MyImage> images) {
		this.images = images;
	}
	public void addImage(MyImage i) {
		this.images.add(i);
	}
	public static String getFolder() {
		return FOLDER;
	}
	
	/**
	    Renvoie l'iterator qui parcourt les images.
	    @return ListIterator itr.
	*/
	public ListIterator<MyImage> getItr() {
		return itr;
	}
	public void setItr(ListIterator<MyImage> itr) {
		this.itr = itr;
	}
	
	/**
	    Renvoie l'image courant visionn�e par l'iterator pr�c�demment (ce dernier �tant entre deux images).
	    @return L'image courante.
	*/
	public MyImage getCurrent() {
		MyImage res = this.images.firstElement();
		if(!this.itr.hasPrevious()) {
			return res;
		}else {
			this.itr.previous();
			return this.itr.next();
		}
	}
}
